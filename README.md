# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:
```
git clone git@bitbucket.org:snofear/stroboskop.git
```

Naloga 6.2.3:

https://bitbucket.org/snofear/stroboskop/commits/670c98af49631bed4850ac4dd6be9534a9ccd161

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:

https://bitbucket.org/snofear/stroboskop/commits/50a57dd82b1f2e8064ce1e0b1d9998d58a54baca?at=izgled

Naloga 6.3.2:

https://bitbucket.org/snofear/stroboskop/commits/0b936ca15ea5b2cdb3d39036c36557020b2a9579?at=izgled

Naloga 6.3.3:

https://bitbucket.org/snofear/stroboskop/commits/d5c932fc689b2162ed75c34ad619977d84de979a?at=izgled

Naloga 6.3.4:

https://bitbucket.org/snofear/stroboskop/commits/b8af3b9db4482180ff09e9be05272aad87a77db4?at=izgled

Naloga 6.3.5:
```
git fetch
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:

https://bitbucket.org/snofear/stroboskop/commits/99f5aa641ac33996ad016933525ff56c381d47f6?at=dinamika

Naloga 6.4.2:

https://bitbucket.org/snofear/stroboskop/commits/f4d6b3c3d43a278ccfc78c2c6927c579ec4a79d4?at=dinamika

Naloga 6.4.3:

https://bitbucket.org/snofear/stroboskop/commits/4601396ed27cea86b2e67baa0ce2c0bc17619d1f?at=dinamika

Naloga 6.4.4:

https://bitbucket.org/snofear/stroboskop/commits/c04671e41baf75b84b14f670076b1182fc568c81